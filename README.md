# Android todo workshop

## Prerequisites

- All code is written in Kotlin
- Views are written in XML and not in Compose. Compose is easier to write in but a lot harder to understand. It's not adopted in the Android market yet and most apps use XML views. There's more documentation for XML.
- Developers do not push to master directly (it is a protected branch). Instead, developers need to create a pull request (merge request) for every task.
- Branches need to be named in the following convention: `X-YY_task_name` where X is the section number and Y task number, e.g. `1-01_change_app_name`
- In each merge request developers should provide a description of changes and a screenshot or a video if it involves UI. 

## First steps

Clone the repo. This is a basic template created by Android studio. Learn what is where. Run it. See if it works. Make some coffee. See declared activities in `AndroidManifest.xml`. Look at dependencies in `app/build.gradle.kts`. Curse at the guy who created this devilish document.

# Tasks 

## UI components basics

In this module you will work mostly with UI, learning about basic components often
used in Android development. Hopefully this will make you aware of how the app is
built from the ground up and how to interact with user interface.

### 1-1 Change app name

Current app name is "Hello, World!". Change it to "Todo workshop". 

Output:
- app name is changed in the Android launcher

Hint:
- 1 file is changed

Reading:
- https://developer.android.com/guide/topics/manifest/manifest-intro
- https://developer.android.com/reference/android/Manifest

![toolbar](images/hello1.png)

### 1-2 Adjust main label

Move the main label from the center of the screen to its top left. Add 16dp margin from all sides. Make the font bigger. Change the label to something else (be creative). String should not be hardcoded but applied from `strings.xml`

Hint:
- 2 files are changed

Reading:
- https://constraintlayout.com/basics/
- https://developer.android.com/guide/topics/resources/string-resource

![toolbar](images/hello2.png)

### 1-3 Add FAB

Add `FloatingActionButton` with a plus icon to the bottom right side of the
screen.

Hint:
- 1 file is changed

Reading:
- https://developer.android.com/develop/ui/views/components/floating-action-button
- https://developer.android.com/reference/com/google/android/material/floatingactionbutton/FloatingActionButton

![toolbar](images/hello3.png)

### 1-4 Toolbar

Add a Toolbar to the bottom of the screen, above the label modified in 1-2. It
should have a label with app name.

Reading:
- https://developer.android.com/reference/android/widget/Toolbar
- https://developer.android.com/develop/ui/views/components/appbar/setting-up

![toolbar](images/hello4.png)

## 1-5 ToDo Model

As a preparation for API integration, create a data model for Tasks and lists.

Spec:

```
Task

+ uuid: String
+ name: String
+ description: String
+ priority: ???
+ tags: List<String>
```

```
Tag

+ uuid: String
+ name: String
+ color: String

+ androidColor(): Color
```

There will be two classes - `Task` and `Tag`. Each will have an unique identifier that should be assigned when the
object is created (it is not part of the constructor!) 

- `Priority` of a task should have 4 values - low priority, medium priority, high priority, critical. It's up to
    you to decide what data structure will best represt in (think about integers/enum values). User should _not_
    be able to create a task with a different priority than one of these. If he tries, you may throw an exception.
- `Tags` is a list of uuids of tags that are assigned to this task. It cannot contain _whole_ `Tag` object, only
    its uuid.
- `Color` in a tag should be represented by a string (RGB)
- `androidColor()` is a function that returns a `Color` object from Android derived from the string representation

Reading:
- [UUID](https://developer.android.com/reference/kotlin/java/util/UUID)
- [Data classes](https://kotlinlang.org/docs/data-classes.html)

## 1-6 Recycler

In this task you will need to create a basic dynamic UI element - a scrollable list. To do so you will use a widget called `RecyclerView` and create a `ViewHolder` for it with a
single element. `ViewHolder` is something like a cell. It is called `RecyclerView` because its views get recycled - when you scroll down, items on top disappear and new show up
from the bottom. To achieve better performance, `RecyclerView` doesn't create a new UI elements every time they need to be shown but re-uses those that are now hidden and changes
the data. tl;dr - when only 7 items will fit on a screen, 7 UI elements will be created and their content will be replaced when needed. It's miles better than the previous widget
on Android called `ListView`. Which sucked. Don't use it.

- Add a `RecyclerView` to the UI filling remaining space (so it should stretch from the label at the top to the bottom of the screen)
- Create UI for a single element that has name, description and priority
- Create an adapter for the List of tasks
- When `MainActivity` is created, add any number of Tasks you want to the list and display them. They will be hardcoded for now.

Hint:
- you will need at least 2 classes: one for view holder and one for the adapter. Read docs.

Reading:
- [RecyclerView](https://developer.android.com/develop/ui/views/layout/recyclerview)
- [Docs](https://developer.android.com/reference/androidx/recyclerview/widget/RecyclerView)

![toolbar](images/hello5.png)

## 1-7 Todo Repository

This is about abstraction and repository-pattern. For now it will be very simple, maybe it won't even make sense but wait for it.

Create an interface called `TaskRepository`. For now it will have a single function `fetchTasks` which will synchronusly fetch some tasks from the backend service and return it. We do not have a backend service yet so create an implementation for that interface called `StubTaskRepository` which will randomly generate specified number of tasks when invoked.

```
TaskRepository

+ fetchTasks(count: Int): List<Task>
```

## 1-8 Repository integration

When `MainActiivty` is created, instantiate new `TaskRepository` with `StubTaskRepository` implementation, generate 10 tasks and display them in the `RecyclerView` created in
1-6.

![toolbar](images/hello6.png)

## 1-9 Colored tasks

Make the background color of each task with the color that was specified in the data object. 

![toolbar](images/hello7.png)

## Quiz

- What is an Android Manifest?
